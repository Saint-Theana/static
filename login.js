"use strict";

$(function () {
    if (window.WebViewJavascriptBridge) {
        registerAPI();
    } else {
        document.addEventListener(
            'WebViewJavascriptBridgeReady'
            , function () {
                //do your work here
                registerAPI();
            },
            false
        );
    }

});


function registerAPI() {
    WebViewJavascriptBridge.registerHandler("webAuth", function (data, responseCallback) {
        var req = JSON.parse(data);
        $.ajax({
            type: 'POST',
            url: "/SQv8/MsgassistantProtocolServer",
            data: {
                uin: req.uin,
                dk: req.dk,
                data: req.data,
                uid: req.uid
            },
            dataType: "json",
            async: true,
            success: function (d) {
                responseCallback(d);
            },
            error: function () {
                responseCallback(JSON.stringify({
                    result: -1,
                    msg: "请求服务器发生错误"
                }));
            }
        });
    });
    //遮罩
    WebViewJavascriptBridge.registerHandler("process", function (data, responseCallback) {
        $.LoadingOverlay(data);
        responseCallback('0');
    });

    WebViewJavascriptBridge.registerHandler("onFail", function (data, responseCallback) {
        responseCallback('0');
        data = data + '\n提示:可能你网络状态不好,重试几次或者重启本软件';
        data = data.replace('\n', '<br>');
        alertSimple('登录失败', data);
    });
    checkAgreement();
    test();
}

function test() {

}

function checkAgreement() {
    readString('agreeVc', function (agreeVc) {
        readString('vc', function (vc) {
            if (agreeVc != vc) {
                location.href = "/SQv8/agreement.html";
            }
        })
    })
}

function onLoginSubmit() {
    var uin = $("#uin").val();
    var pwd = $("#pwd").val();
    var protocol = $("#protocol").val();

    if (!/^[0-9]{5,11}$/.test(uin)) {
        alertSimple("提示", "QQ输入有误");
    } else if (!/^\S{6,}$/.test(pwd)) {
        alertSimple("提示", "密码输入有误");
    } else if (!/^(padqq)|(androidqq)|(macqq)$/.test(protocol)) {
        alertSimple("提示", "在线类型有误");
    } else {


        //login
        doLogin(uin, pwd, protocol);

    }
    return false;
}

function doLogin(uin, pwd, protocol) {
    WebViewJavascriptBridge.callHandler(
        'startLogin'
        ,
        {
            'uin': uin,
            'pwd': pwd,
            'protocol': protocol,
            'timeout': 10 * 1000
        }
        , function (responseData) {
            if (responseData.length > 0) {
                $.LoadingOverlay("show");
                writeString(uin, pwd);
            }
        }
    );
}


/**
 * 清空输入
 */
function clearInput() {
    $("#uin").val("");
    $("#pwd").val("");
    $("#protocol").val("");
}


function alertSimple(title, msg) {
    $("#exampleModalLongTitle").text(title);
    $("#simpleMsg").html(msg);
    $("#exampleModalCenter").modal("show");
}


function onSelectProtocol(protocol) {
    var txt = "该型号将会把手机QQ挤下线";
    if (protocol.indexOf("androidqq") == 0) {
        // $("#protocolWarn").css("display", "inline");
        txt = "该型号支持抢红包但会把手机QQ挤下线"
    } else {
        // $("#protocolWarn").css("display", "none");
        txt = "";
    }
    $("#protocolWarn").text(txt);

}


function onUserChange(user) {
    if (/^[0-9]{5,11}$/.test(user)) {
        $("#userFace").attr("src", "http://q.qlogo.cn/headimg_dl?dst_uin=" + user + "&spec=100");
        readString(user, function (s) {
            $("#pwd").val(s);
        });
    }
}
