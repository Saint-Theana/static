"use strict";

function readString(key, callback) {
    if (WebViewJavascriptBridge) {
        WebViewJavascriptBridge.callHandler(
            'readString'
            ,
            {key: key}
            , callback
        );
    }
}

function writeString(key, value, callback) {
    if (WebViewJavascriptBridge) {
        WebViewJavascriptBridge.callHandler(
            'writeString'
            ,
            {
                key: key,
                value: value
            }
            , callback
        );
    }
}

function getDk(callback) {
    if (WebViewJavascriptBridge) {
        WebViewJavascriptBridge.callHandler(
            'getdk'
            ,
            {}
            , callback
        );
    }
}

function getDkOnly(callback) {
    if (WebViewJavascriptBridge) {
        WebViewJavascriptBridge.callHandler(
            'getdkOnly'
            ,
            {}
            , callback
        );
    }
}

function writeDk(dk, callback) {
    if (WebViewJavascriptBridge) {
        WebViewJavascriptBridge.callHandler(
            'writeDk'
            ,
            {
                dk: dk
            }
            , callback
        );
    }
}

function exit() {
    if (WebViewJavascriptBridge) {
        WebViewJavascriptBridge.callHandler(
            'exit'
            ,
            {}
        );
    }
}

function finish() {
    if (WebViewJavascriptBridge) {
        WebViewJavascriptBridge.callHandler(
            'finish'
            ,
            {}
        );
    }
}

function openWeb(url) {
    if (WebViewJavascriptBridge) {
        WebViewJavascriptBridge.callHandler(
            'openWebSite',
            {
                'url': url,
                'type': 0
            }
        );
    }

}


function joinGroup(key) {
    if (WebViewJavascriptBridge) {
        WebViewJavascriptBridge.callHandler(
            'openWebSite',
            {
                'url': key,
                'type': 1
            }
        );
    }

}

function joinQQ(uin) {
    if (WebViewJavascriptBridge) {
        WebViewJavascriptBridge.callHandler(
            'openWebSite',
            {
                'url': uin,
                'type': 2
            }
        );
    }

}

function downloadApk(url, useInner) {
    if (WebViewJavascriptBridge) {
        WebViewJavascriptBridge.callHandler(
            'downApk',
            {
                'url': url,
                'type': useInner ? 0 : 1,
                'suffix':'.apk'
            }
        );
    }
}

function getUid(callback) {
    if (WebViewJavascriptBridge) {
        WebViewJavascriptBridge.callHandler(
            'getUid'
            ,
            {}
            , callback
        );
    }
}
